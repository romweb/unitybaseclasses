﻿using UnityEngine;
using System.Collections;
using UnityBaseClasses;

namespace UnityBaseClasses
{
    /// <summary>
    ////Привязывает объект к краям физического экрана устройства
    /// </summary>
    public class ScreenReltaivePosition : MonoBehaviourBase
    {

        public enum verticalEdge { top, bottom, nochange };
        public enum horizontalEdge { left, right, nochange };

        /// <summary>
        /// Позиция привязки по вертикали
        /// </summary>
        [Header("Screen Reltaive Position settings")]
        [Tooltip("Vertical edge")]
        public verticalEdge screenVerticalEdge;
        /// <summary>
        /// Позиция привязки по горизонтали
        /// </summary>
        [Tooltip("Horizontal edge")]
        public horizontalEdge screenHorizontalEdge;
        /// <summary>
        /// Смещение по оси Y для привязки
        /// </summary>
        [Tooltip("Vertical offset")]
        public float yOffset;
        /// <summary>
        /// Смещение по оси X для привязки
        /// </summary>
        [Tooltip("Horizontal offset")]
        public float xOffset;

        /// <summary>
        /// Ссылка на камеру, которая используется для расчёта координат
        /// </summary>
        [Tooltip("Camera for assign the coordinates")]
        public Camera mainCamera;

	    void Start () {

            if (mainCamera == null)
                Debug.LogError("ScreenReltaivePosition.mainCamera isn't defined!");

            Vector3 newPosition = myTransform.position;

            // рассчитываем координаты по горизонтали
            switch (screenHorizontalEdge)
            {
                case horizontalEdge.right:
                    newPosition.x = mainCamera.aspect * mainCamera.orthographicSize + xOffset;
                    break;
                case horizontalEdge.left:
                    newPosition.x = -mainCamera.aspect * mainCamera.orthographicSize + xOffset;
                    break;
            }

            // рассчитываем координаты по вертикали
            switch (screenVerticalEdge)
            {
                case verticalEdge.top:
                    newPosition.y = mainCamera.orthographicSize + yOffset;
                    break;
                case verticalEdge.bottom:
                    newPosition.y = -mainCamera.orthographicSize + yOffset;
                    break;
            }

            // смещаем
            transform.position = newPosition;
	    }
    }
}
