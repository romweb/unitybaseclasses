﻿// Copyright (c) by Roman Kondratiev
// http://romweb.ru
// kondratiev@gmail.com

using System;

namespace UnityBaseClasses
{

    /// <summary>
    /// Расширенная работа с строками
    /// </summary>
    public class StringEx
    {
        /// <summary>
        /// Форматирование строки
        /// </summary>
        /// <param name="text">Текст (м.б. форматным или нет)</param>
        /// <param name="parameters">Параметры</param>
        /// <returns>Строка с подставленными параметрами</returns>
        public static string FormatMessage(string text, params object[] parameters)
        {
            string textValue = null;
            if (parameters.Length == 0)
                textValue = text;
            else
            {
                try
                {
                    textValue = String.Format(text, parameters);
                }
                catch (FormatException)
                {
                }
            }

            return textValue;
        }
    }
}