﻿using UnityEngine;
using System.Collections;
using UnityBaseClasses;

namespace UnityBaseClasses
{
    class CamFollow : MonoBehaviourBase
    {
        public Transform target;
        public float movingDelta = 10f;
        private Vector3 positionDelta = Vector3.zero;

        void FixedUpdate()
        {
            myTransform.position = Vector3.Slerp(myTransform.position, new Vector3(target.position.x, myTransform.position.y, myTransform.position.z), movingDelta * Time.fixedDeltaTime);
        }
    }
}
