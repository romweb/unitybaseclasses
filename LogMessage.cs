﻿// Copyright (c) by Roman Kondratiev
// http://romweb.ru
// kondratiev@gmail.com

using System;
using System.Collections;
using UnityEngine;
using UnityBaseClasses;

namespace UnityBaseClasses
{
    /// <summary>
    /// Вывод лога
    /// </summary>
    public class LogMessage : MonoBehaviourBase
    {
        /// <summary>
        /// Свойство - разрешить/запретить лог
        /// </summary>
        public bool Enable { get; set; }

        /// <summary>
        /// Вывод сообщения
        /// </summary>
        /// <param name="text">Текст (м.б. форматным или нет)</param>
        /// <param name="parameters">Параметры</param>
        public void Message(string text, params object[] parameters)
        {
            if (Enable)
            {
                string textValue = StringEx.FormatMessage(text, parameters);
                if (!String.IsNullOrEmpty(textValue))
                    Debug.Log(textValue);
            }
        }

        /// <summary>
        /// Вывод ошибки
        /// </summary>
        /// <param name="text">Текст (м.б. форматным или нет)</param>
        /// <param name="parameters">Параметры</param>
        public void Error(string text, params object[] parameters)
        {
            if (Enable)
            {
                string textValue = StringEx.FormatMessage(text, parameters);
                if (!String.IsNullOrEmpty(textValue))
                    Debug.LogError(textValue);
            }
        }

        /// <summary>
        /// Вывод предупреждения
        /// </summary>
        /// <param name="text">Текст (м.б. форматным или нет)</param>
        /// <param name="parameters">Параметры</param>
        public void Warning(string text, params object[] parameters)
        {
            if (Enable)
            {
                string textValue = StringEx.FormatMessage(text, parameters);
                if (!String.IsNullOrEmpty(textValue))
                    Debug.LogWarning(textValue);
            }
        }

        /// <summary>
        /// Вывод эксепшена
        /// </summary>
        /// <param name="e">Эксепшен</param>
        /// <param name="context">Объект</param>
        public void Exception(Exception e, UnityEngine.Object context = null)
        {
            if (Enable)
            {
                if (context != null)
                    Debug.LogException(e, context);
                else
                    Debug.LogException(e);
            }
        }
    }
}
