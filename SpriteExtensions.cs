﻿using UnityEngine;
using System.Collections;
using UnityBaseClasses;

namespace UnityBaseClasses
{
    /// <summary>
    /// Дополнительные функции для спрайтов
    /// </summary>
    public class SpriteExtensions : MonoBehaviourBase
    {
        public enum Facing { Right, Left };

        Facing facing = Facing.Left;

        /// <summary>
        /// Зеркальное отражение спрайта вправо
        /// </summary>
        public void FlipRight()
        {
            if (facing == Facing.Left)
                FlipVertically();
        }

        /// <summary>
        /// Зеркальное отражение спрайта влево
        /// </summary>
        public void FlipLeft()
        {
            if (facing == Facing.Right)
                FlipVertically();
        }

        /// <summary>
        /// Зеркальное отражение по вертикали
        /// </summary>
        public void FlipVertically()
        {
            Vector3 theScale = myTransform.localScale;
            theScale.x *= -1;
            myTransform.localScale = theScale;

            if (facing == Facing.Left)
                facing = Facing.Right;
            else
                facing = Facing.Left;
        }

        /// <summary>
        /// Возвращает направление перемещения
        /// </summary>
        /// <param name="target">Координаты цели</param>
        /// <returns>Направление</returns>
        public Vector3 GetDirection(Vector3 target)
        {
            var heading = target - myTransform.position;
            var distance = heading.magnitude;
            return (heading / distance);
        }

        /// <summary>
        /// Возвращает направление перемещения по вертикали
        /// </summary>
        /// <param name="target">Координаты цели</param>
        /// <returns>Направление</returns>
        public float GetVerticalDirection(Vector3 target)
        {
            Vector3 direction = GetDirection(target);
            return direction.y;
        }
        /// <summary>
        /// Возвращает направление перемещения по горизонтали
        /// </summary>
        /// <param name="target">Координаты цели</param>
        /// <returns>Направление</returns>
        public float GetHorizontalDirection(Vector3 target)
        {
            Vector3 direction = GetDirection(target);
            return direction.x;
        }

        /// <summary>
        /// Возвращает дистанцию до точки
        /// </summary>
        /// <param name="target">Объект-цель</param>
        /// <returns>Дистанция</returns>
        public float GetDistance(Transform target)
        {
            var heading = target.position - myTransform.position;
            return heading.magnitude;
        }
    }
}
