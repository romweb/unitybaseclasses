﻿// Copyright (c) by Roman Kondratiev
// http://romweb.ru
// kondratiev@gmail.com

using UnityEngine;
using System.Collections;

namespace UnityBaseClasses
{

    /// <summary>
    /// Используется для кэширования объектов типа Transform
    /// </summary>
    public class MonoBehaviourBase : MonoBehaviour
    {
        public void _Awake()
        {
            myTransform = transform;
            myAnimator = GetComponent<Animator>();
            mySpriteRenderer = GetComponent<SpriteRenderer>();
            myParticleSystem = GetComponent<ParticleSystem>();
            myRigidbody2D = GetComponent<Rigidbody2D>(); ;
            myRenderer = GetComponent<Renderer>(); ;
            myAudio = GetComponent<AudioSource>();
            thisCamera = Camera.main;

#if !UNITY_EDITOR
            // для того чтобы пыла пауза в игре, если её свернуть
            Application.runInBackground = false;
#endif
        }

        void Awake()
        {
            _Awake();
        }

        [HideInInspector]
        public Transform myTransform;
        [HideInInspector]
        public Animator myAnimator;
        [HideInInspector]
        public ParticleSystem myParticleSystem;
        [HideInInspector]
        public Rigidbody2D myRigidbody2D;
        [HideInInspector]
        public Renderer myRenderer;
        [HideInInspector]
        public AudioSource myAudio;
        [HideInInspector]
        public Camera thisCamera;
        [HideInInspector]
        public SpriteRenderer mySpriteRenderer;

        public IEnumerator Wait(float time)
        {
            yield return new WaitForSeconds(time);
        }

        /// <summary>
        /// Установка sort order для всех объектов на одной горизонтальной линии
        /// </summary>
        /// <param name="position">Стартовая позиция</param>
        /// <param name="mask">Маска объектов</param>
        /// <param name="index">Устанавливаемый номер sort order </param>
        public void SetItemsSortOrder(Vector3 position, Vector3 direction, string mask, int index)
        {
            RaycastHit2D[] items = Physics2D.RaycastAll(position, direction, Mathf.Infinity, 1 << LayerMask.NameToLayer(mask));
            foreach (RaycastHit2D item in items)
            {
                SpriteRenderer sprite = item.transform.gameObject.GetComponent<SpriteRenderer>();
                if (sprite)
                    sprite.sortingOrder = index;
            }
        }

        /// <summary>
        /// Задание объекту размеров ортографической камеры
        /// </summary>
        /// <param name="camera">Камера из которой берутся размеры</param>
        public void ResizeToCamera(Camera camera)
        {
            myTransform.localScale = new Vector3(camera.aspect * camera.orthographicSize * 2, camera.orthographicSize * 2, 1);
        }

        public enum CameraCorner {TopLeft, TopRight, BottomLeft, BottomRight};

        public Vector3 CameraCornerToWorldPoint(Vector3 delta, Camera camera, CameraCorner corner, float distance = 1f)
        {
            Vector3 res = Vector3.zero;

            switch (corner)
            {
                case CameraCorner.TopLeft:
                    res = camera.ScreenToWorldPoint(new Vector3(0f, 0f, distance));
                    break;
                case CameraCorner.TopRight:
                    res = camera.ScreenToWorldPoint(new Vector3(camera.pixelWidth, 0f, distance));
                    break;
                case CameraCorner.BottomLeft:
                    res = camera.ScreenToWorldPoint(new Vector3(0f, camera.pixelHeight, distance));
                    break;
                case CameraCorner.BottomRight:
                    res = camera.ScreenToWorldPoint(new Vector3(camera.pixelWidth, camera.pixelHeight, distance));
                    break;
            }

            res -= delta;

            return res;
        }
    }
}