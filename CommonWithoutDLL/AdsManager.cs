﻿using UnityEngine;
using System;
using System.Collections;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public class AdsManager : MonoBehaviour
{
    public enum InterstitialClosingType { Quit, Reload };

    [HideInInspector]
    public static AdsManager instance;    // Patron Singleton

    public bool useBottomBanner = false;
    public bool useInterstitialBanner = false;

    public bool useInterstitialWithPeriod = false;

    public InterstitialClosingType interstitialClosingType = InterstitialClosingType.Reload;

    public string androidAdsBannerId = null;
    public string androidAdsInterstitialBannerId = null;
    
    public bool useTestDevice = false;
    public string androidTestDevice = null;

    private BannerView bannerView = null;
    private InterstitialAd interstitial;
    private bool interstitialLoaded = false;

    private int prevHashCode = -1;

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        if (useBottomBanner)
        {
            bannerView = new BannerView(androidAdsBannerId, AdSize.SmartBanner, AdPosition.Bottom);

            AdRequest request = null;
            if (useTestDevice)
                request = new AdRequest.Builder()
                    .AddTestDevice(AdRequest.TestDeviceSimulator)  
                    .AddTestDevice(androidTestDevice)
                    .Build();
            else
                request = new AdRequest.Builder().Build();
            if (request != null)
                bannerView.LoadAd(request);
        }

        LoadInterstitialBanner();
    }

    public void LoadInterstitialBanner()
    {
        if (useInterstitialBanner)
        {
            interstitialLoaded = false;

            if (interstitial != null)
            {
                interstitial.Destroy();
                interstitial = null;
            }

            interstitial = new InterstitialAd(androidAdsInterstitialBannerId);
            if (interstitial != null)
            {
                interstitial.AdClosed += HandleInterstitialClosed;
                interstitial.AdLoaded += HandleInterstitialLoaded;

                AdRequest request = null;
                if (useTestDevice)
                    request = new AdRequest.Builder()
                        .AddTestDevice(AdRequest.TestDeviceSimulator)  
                        .AddTestDevice(androidTestDevice).Build();
                else
                    request = new AdRequest.Builder().Build();

                if (request != null)
                    interstitial.LoadAd(request);
            }
        }
    }

    public void StartInterstitialPeriodBanner(float timesec)
    {
        if (useInterstitialWithPeriod && useInterstitialBanner)
            InvokeRepeating("Invoke_InterstitialPeriod", timesec, timesec);
    }

    public void ShowBanner(bool show = true)
    {
        if (bannerView != null)
        {
            if (show)
                bannerView.Show();
            else
                bannerView.Hide();
        }
    }

    public void ShowInterstitial()
    {
        ShowBanner(false);

        if (interstitial != null)
        {
            if (interstitial.IsLoaded())
            {
                if (prevHashCode == -1 || prevHashCode != interstitial.GetHashCode())
                {
                    prevHashCode = interstitial.GetHashCode();
                    interstitial.Show();
                }
            }
        }
    }

    public bool IsInterstitialLoaded()
    {
        return (interstitial != null) && interstitialLoaded;
    }

    public void HandleInterstitialClosed(object sender, EventArgs args)
    {
        switch (interstitialClosingType)
        {
            case InterstitialClosingType.Reload:
                LoadInterstitialBanner();
                break;
            case InterstitialClosingType.Quit:
                Application.Quit();
                break;
        }
    }

    public void HandleInterstitialLoaded(object sender, EventArgs args)
    {
        interstitialLoaded = true;
    }

    void OnDestroy()
    {
        if (interstitial != null)
            interstitial.Destroy();
        if (bannerView != null)
            bannerView.Destroy();
    }

    void Invoke_InterstitialPeriod()
    {
        ShowInterstitial();
    }
}