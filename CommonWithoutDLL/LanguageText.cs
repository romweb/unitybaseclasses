﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LanguageText : MonoBehaviour 
{
    Text text;

	void Start ()
    {
        text = GetComponent<Text>();
        if (text != null)
            text.text = LanguageManager.GetText(text.text);
    }
}
