﻿// Copyright (c) by Roman Kondratiev
// http://romweb.ru
// kondratiev@gmail.com

using UnityEngine;
using System;
using System.Collections;
using JsonFx.Json;

namespace UnityBaseClasses
{
    /// <summary>
    /// Базовый класс с параметрами игры
    /// </summary>
    [System.Serializable]
    public class GameOptionsBase
    {
        /// <summary>
        /// Версия игры
        /// </summary>
        public string Version;
        /// <summary>
        /// Использовать игровые звуковые эффекты ил нет?
        /// </summary>
        public string SoundOn;
        /// <summary>
        /// Играть фоновую музыку или нет?
        /// </summary>
        public string MusicOn;
        /// <summary>
        /// Текущий язык интерфейса
        /// </summary>
        public string Language;
        /// <summary>
        /// Первый старт игры или нет?
        /// </summary>
        public string FirstStart;
        /// <summary>
        /// Текущий пользователь
        /// </summary>
        public string Username;
        /// <summary>
        /// Лучший рекорд пользователя
        /// </summary>
        public int PersonalRecord;
        /// <summary>
        /// Разрешено отзыв или нет
        /// </summary>
        public string RateEnable;
        /// <summary>
        /// Отзыв  сделан или нет
        /// </summary>
        public string RateComplete;
    }

    /// <summary>
    /// Работа с параметрами игры
    /// </summary>
    /// <typeparam name="T">Класс, описывающий переменные параметров игры. По-умолчанию можно использовать GameOptionsBase.</typeparam>
    public class GameOptions<T>
    {
        private string keyName = null;
        private string gameVersion;
        public T values;

        /// <summary>
        /// Инициализация
        /// </summary>
        /// <param name="version">Версия игры</param>
        /// <param name="keyName">Имя ключа в PlayerPrefs для настроек игры</param>
        public void Initialization(string version, string keyName)
        {
            this.gameVersion = version;
            this.keyName = keyName;
        }

        /// <summary>
        /// Чтение параметров по-умолчанию из файла в Resources
        /// </summary>
        /// <param name="keyName">Имя ключа в PlayerPrefs для настроек игры</param>
        private void ReloadDefaults(string keyName)
        {
            TextAsset jsonFile = Resources.Load(keyName, typeof(TextAsset)) as TextAsset;
            PlayerPrefs.SetString(keyName, jsonFile.text);
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Получение версии игры, сохранённой в PlayerPrefs
        /// </summary>
        /// <param name="options">Параметры, прочитанные из PlayerPrefs</param>
        /// <returns>Версия игры</returns>
        virtual public string GetVersionFromRegistry(T options)
        {
            // Получаем версию параметров игры, сохранённую в настройках
            GameOptionsBase baseOptions = new GameOptionsBase();
            baseOptions = options as GameOptionsBase;

            return baseOptions.Version;
        }

        /// <summary>
        /// Чтение параметров игры
        /// </summary>
        /// <returns>Успешно или нет чтение</returns>
        public bool ReadGameParams()
        {
            bool res = false;

            try
            {
                // если в реестре нет ключа keyName, то создаём его параметрами по-умолчанию (берем из папки Resources Unity)
                if (!PlayerPrefs.HasKey(keyName))
                    ReloadDefaults(keyName);

                bool invalidOptions = true;

                do
                {
                    // Получаем json строку с параметрами
                    string data = PlayerPrefs.GetString(keyName);
                    var optionsReader = new JsonReader();

                    // Сериализуем их в класс
                    values = optionsReader.Read<T>(data);

                    string version = GetVersionFromRegistry(values);
                    // Если версия не соответсвует версии игры, то пересоздаём параметры параметрами по умолчания
                    if (version == null || version != gameVersion)
                        ReloadDefaults(keyName);
                    else
                        invalidOptions = false; // иначе - выходим из цикла

                } while (invalidOptions);

                res = true;
            }

            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return res;
        }

        /// <summary>
        /// Сохранение параметров игры
        /// </summary>
        /// <returns>Успешно или нет сохранение</returns>
        public bool SaveGameParams()
        {
            bool res = false;

            try
            {
                var optionsWriter = new JsonWriter();
                string data = optionsWriter.Write(values);
                PlayerPrefs.SetString(keyName, data);
                PlayerPrefs.Save();
                res = true;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return res;
        }
    }
}