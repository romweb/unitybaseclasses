﻿// Copyright (c) by Roman Kondratiev
// http://romweb.ru
// kondratiev@gmail.com

using System;

namespace UnityBaseClasses
{
    [System.Serializable]
    public class IntVector2
    {
        public int x = 0;
        public int y = 0;

        public IntVector2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public bool IsError()
        {
            return (x == -1 && y == -1);
        }

        static public IntVector2 error
        {
            get
            {
                return new IntVector2(-1, -1);
            }
        }

        static public IntVector2 zero
        {
            get
            {
                return new IntVector2(0, 0);
            }
        }
    }
}
