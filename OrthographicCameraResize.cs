﻿// Copyright (c) by Roman Kondratiev
// http://romweb.ru
// kondratiev@gmail.com

using UnityEngine;
using System.Collections;
using UnityBaseClasses;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace UnityBaseClasses
{
    /// <summary>
    /// Устанавливает размер камеры равным половине высоты экрана
    /// </summary>
#if UNITY_EDITOR
    [InitializeOnLoad]
#endif
    public class OrthographicCameraResize : MonoBehaviour
    {
        /// <summary>
        /// Параметры камеры
        /// </summary>
        [System.Serializable]
        public class CameraParams
        {
            public CameraParams(float aspect, float size, float scaleFactor)
            {
                Set(aspect, size, scaleFactor);
            }

            public float aspect = 0f;
            public float size = 0f;
            public float scaleFactor = 0f;

            public void Set(float aspect, float size, float scaleFactor)
            {
                this.aspect = aspect;
                this.size = size;
                this.scaleFactor = scaleFactor;
            }
        }

        [Header("Настройки камеры")]
        /// <summary>
        /// Массив размеров камеры для фона 2048x1152, для разного соотношения сторон
        /// </summary>
        [Tooltip("Массив размеров камеры для разного соотношения сторон")]
        public CameraParams[] cameraSizes = new CameraParams[9] {
            new CameraParams (1.25f, 6.76f, 0f),
            new CameraParams (1.33f, 6.41f, 0f),
            new CameraParams (1.5f, 5.61f, 0f),
            new CameraParams (1.6f, 5.24f, 0f),
            new CameraParams (1.78f, 4.75f, 0f),
            new CameraParams (1.77f, 4.75f, 0f),
            new CameraParams (1.7f, 4.96f, 0f),
            new CameraParams (1.71f, 4.92f, 0f),
            new CameraParams (1.67f, 5.07f, 0f)
        };

        /// <summary>
        /// Камера, для которой изменяется размер
        /// </summary>
        [Tooltip("Ссылка на камеру, для которой изменяется размер")]
        public Camera cameraRef = null;

        void OnEnable()
        {
            if (cameraRef == null)
            {
                Debug.LogError("OrthographicCameraResize.cameraRef isn't defined!");
                enabled = false;
            }
        }

        /// <summary>
        /// Получение соотношения экрана и установка оптимального размера камеры
        /// </summary>
        void Start()
        {
            float aspect = Mathf.Round(((float)Screen.width / (float)Screen.height)*100f) / 100f;

            foreach (CameraParams param in cameraSizes)
            {
                if (Mathf.Approximately(param.aspect, aspect))
                {
                    if (cameraRef)
                        cameraRef.orthographicSize = param.size - param.scaleFactor;
                    break;
                }
            }
        }
    }
}

