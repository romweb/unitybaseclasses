﻿// Copyright (c) by Roman Kondratiev
// http://romweb.ru
// kondratiev@gmail.com


using UnityEngine;
using System.Collections;
using UnityBaseClasses;

namespace UnityBaseClasses
{
    public class Raycast2DEx<T> : MonoBehaviour
    {
        /// <summary>
        /// Возможные типы направлений поиска объектов
        /// </summary>
        [HideInInspector]
        public enum SearchType { Left, Right, Top, Bottom, LeftTop, TopBottom, RightTop, RightBottom };
    }
}