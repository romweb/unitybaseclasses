﻿using UnityEngine;
using System.Collections;
using UnityBaseClasses;

namespace UnityBaseClasses
{
    /// <summary>
    /// Определение нахождения объекта в определённых границах
    /// </summary>
    [RequireComponent(typeof(BoxCollider2D))]
    public class LimitRegion : MonoBehaviourBase
    {
        /// <summary>
        /// Имя тега объекта, положение которого контролируется в регионе
        /// </summary>
        public string objectTag;
        /// <summary>
        /// Флаг нахождения объекта objectTag внутри региона
        /// </summary>
        private bool isRegion = false;

        void Start()
        {
            if (string.IsNullOrEmpty(objectTag))
                Debug.LogError("LevelRegion.objectTag isn't defined!");
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (string.Compare(objectTag, other.tag) == 0)
                isRegion = true;
        }

        void OnTriggerExit2D(Collider2D other)
        {
            if (string.Compare(objectTag, other.tag) == 0)
                isRegion = false;
        }

        public bool ObjectInRegion()
        {
            return isRegion;
        }
    }
}