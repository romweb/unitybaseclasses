﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace UnityBaseClasses
{
    /// <summary>
    /// Карта уровня 2D игры
    /// </summary>
    /// <typeparam name="T">Класс, который описывает позицию карты</typeparam>
    public class AStarMap2D<T>
    {
        protected static int aStarDistance = 0;

        /// <summary>
        /// Класс 2D Grid Based AI, который обеспечивает возможность перемещения по карте уровня с использованием А*
        /// </summary>
        /// <typeparam name="TPathNode"></typeparam>
        /// <typeparam name="TUserContext"></typeparam>
        public class MySolver<TPathNode, TUserContext> : SettlersEngine.SpatialAStar<TPathNode,
        TUserContext> where TPathNode : SettlersEngine.IPathNode<TUserContext>
        {
            protected override Double Heuristic(PathNode inStart, PathNode inEnd)
            {
                int formula = aStarDistance;
                int dx = Math.Abs(inStart.X - inEnd.X);
                int dy = Math.Abs(inStart.Y - inEnd.Y);

                if (formula == 0)
                    return Math.Sqrt(dx * dx + dy * dy); //Euclidean distance
                else if (formula == 1)
                    return (dx * dx + dy * dy); //Euclidean distance squared
                else if (formula == 2)
                    return Math.Min(dx, dy); //Diagonal distance
                else if (formula == 3)
                    return (dx * dy) + (dx + dy); //Manhatten distance
                else
                    return Math.Abs(inStart.X - inEnd.X) + Math.Abs(inStart.Y - inEnd.Y);
            }

            protected override Double NeighborDistance(PathNode inStart, PathNode inEnd)
            {
                return Heuristic(inStart, inEnd);
            }

            public MySolver(TPathNode[,] inGrid)
                : base(inGrid)
            {
            }
        };

        /// <summary>
        /// Узел карты
        /// </summary>
        public class PathNode : SettlersEngine.IPathNode<T>
        {
            /// <summary>
            /// Позиция по Х
            /// </summary>
            public Int32 X { get; set; }
            /// <summary>
            /// Позиция по У
            /// </summary>
            public Int32 Y { get; set; }
            /// <summary>
            /// Занята позиция объектом или нет
            /// </summary>
            public Boolean IsWall { get; set; }
            /// <summary>
            /// Параметры объекта
            /// </summary>
            public T data = default(T);

            public bool IsWalkable(T inContext)
            {
                return !IsWall;
            }
        }

        /// <summary>
        /// Карта
        /// </summary>
        public PathNode[,] datas = null;
        /// <summary>
        /// Свободные ячейки карты
        /// </summary>
        protected List<IntVector2> availableCell = null;
        /// <summary>
        /// Размер карты
        /// </summary>
        protected IntVector2 size = IntVector2.zero; 

        /// <summary>
        /// Количество доступных ячеек карты
        /// </summary>
        /// <returns>Количество доступных ячеек карты</returns>
        public int GetAvailableCellCount()
        {
            return availableCell.Count;
        }

        /// <summary>
        /// Инициализация
        /// </summary>
        /// <param name="xSize">Размер по горизонтали</param>
        /// <param name="ySize">Размер по вертекали</param>
        virtual public void Initialization(int xSize, int ySize)
        {
            size.x = xSize;
            size.y = ySize;

            datas = new PathNode[xSize, ySize];
            availableCell = new List<IntVector2>();

            for (int i = 0; i < xSize; i++)
            {
                for (int j = 0; j < ySize; j++)
                {
                    datas[i, j] = new PathNode();
                    availableCell.Add(new IntVector2(i, j));
                }
            }
        }

        /// <summary>
        /// Очистка карты
        /// </summary>
        public void RemoveAll()
        {
            availableCell.Clear();
            Array.Clear(datas, 0, datas.Length);
        }

        /// <summary>
        /// Получение индекса свободной ячейки - рандом
        /// </summary>
        /// <returns>Свободная ячейка</returns>
        public IntVector2 GetRandomIndex()
        {
            IntVector2 res = IntVector2.error;
            try
            {
                if (availableCell.Count > 0)
                {
                    int index = Mathf.RoundToInt(UnityEngine.Random.Range(0, availableCell.Count));
                    res = availableCell[index];
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return res;
        }

        /// <summary>
        /// Изменение ячейки карты
        /// </summary>
        /// <param name="pos">Позиция в карте</param>
        /// <param name="data">Новое значение</param>
        /// <param name="needRemove">Удалить или добавить в массив свободных ячеек</param>
        /// <returns></returns>
        virtual public bool ChangeItem(IntVector2 pos, bool needRemove)
        {
            return ChangeItem(pos.x, pos.y, needRemove);
        }

        /// <summary>
        /// Изменение ячейки карты
        /// </summary>
        /// <param name="xPos">Позиция в карте</param>
        /// <param name="yPos">Позиция в карте</param>
        /// <param name="data">Новое значение</param>
        /// <param name="needRemove">Удалить или добавить в массив свободных ячеек</param>
        /// <returns></returns>
        virtual public bool ChangeItem(int xPos, int yPos, bool needRemove)
        {
            bool res = false;
            try
            {
                if (needRemove)
                {
                    IntVector2 tmp = availableCell.Find(item => (item.x == xPos && item.y == yPos));
                    if (tmp != null)
                    {
                        availableCell.Remove(tmp); // ячейка занята
                        addWall(xPos, yPos);
                    }
                    res = true;
                }
                else
                {
                    HideItem(xPos, yPos); // ячейка свободна
                    res = true;
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return res;
        }

        /// <summary>
        /// Получение значения ячейки карты
        /// </summary>
        /// <param name="xPos">Позиция по Х</param>
        /// <param name="yPos">Позиция по У</param>
        /// <returns>Значение ячейки</returns>
        public T GetItem(int xPos, int yPos)
        {
            T res = default(T);
            try
            {
                res = datas[xPos, yPos].data;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return res;
        }

        /// <summary>
        /// Получение значения ячейки карты
        /// </summary>
        /// <param name="pos">Позиция</param>
        /// <returns>Значение ячейки</returns>
        public T GetItem(IntVector2 pos)
        {
            return GetItem((int)pos.x, (int)pos.y);
        }

        /// <summary>
        /// Скрыть элемент карты
        /// </summary>
        /// <param name="xPos">Позиция по Х</param>
        /// <param name="yPos">Позиция по У</param>
        /// <returns>Успех/нет</returns>
        public bool HideItem(int xPos, int yPos)
        {
            bool res = false;
            try
            {
                availableCell.Add(new IntVector2(xPos, yPos));
                removeWall(xPos, yPos);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return res;
        }

        /// <summary>
        /// Скрыть элемент карты
        /// </summary>
        /// <param name="pos">Позиция</param>
        /// <returns>Успех/нет</returns>
        public bool HideItem(IntVector2 pos)
        {
            return HideItem((int)pos.x, (int)pos.y);
        }

        /// <summary>
        /// Добавление в карту элемента (точка карты занята им)
        /// </summary>
        /// <param name="x">Позиция по Х в карте</param>
        /// <param name="y">Позиция по У в карте</param>
        protected void addWall(int x, int y)
        {
            datas[x, y].IsWall = true;
        }

        /// <summary>
        /// Удаление из карты элемента
        /// </summary>
        /// <param name="x">Позиция по Х в карте</param>
        /// <param name="y">Позиция по У в карте</param>
        protected void removeWall(int x, int y)
        {
            datas[x, y].IsWall = false;
        }
    }
}

