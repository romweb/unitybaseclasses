﻿using UnityEngine;
using System.Collections;

namespace UnityBaseClasses
{

    /// <summary>
    /// Зум ортографической камеры (клавиатура, мышь) 
    /// </summary>
    public class CameraZoom : MonoBehaviour
    {
        /// <summary>
        /// Нормальный size ортографической камеры 
        /// </summary>
        [Header("Camera zoom settings")]
        [Tooltip("Initial size of the camera")]
        public float normalOrthographicSize = 5.8f;
        /// <summary>
        /// Максимальный size ортографической камеры 
        /// </summary>
        [Tooltip("Maximal size of the camera")]
        public float maxOrthographicSize = 7.68f;
        /// <summary>
        /// Минимальный size ортографической камеры 
        /// </summary>
        [Tooltip("Minimal size of the camera")]
        public float minOrthographicSize = 5.8f;
        /// <summary>
        /// Шаг изменения размера ортографической камеры 
        /// </summary>
        [Tooltip("Step resizing camera")]
        public float step = 5.0f;

        private float orthographicSize = .0f;

        void Start()
        {
            orthographicSize = GetComponent<Camera>().orthographicSize = normalOrthographicSize;
        }

        /// <summary>
        /// Изменение зума камеры на шаг, заданный в настройках скрипта
        /// </summary>
        /// <param name="direction">Направление зума</param>
        public void Zoom(float direction)
        {
            // рассчитываем новый размер камеры и проверяем границы
            orthographicSize -= direction * step * Time.deltaTime;
            if (orthographicSize >= maxOrthographicSize)
                orthographicSize = maxOrthographicSize;
            else if (orthographicSize <= minOrthographicSize)
                orthographicSize = minOrthographicSize;

            // задаём новый размер камеры
            GetComponent<Camera>().orthographicSize = orthographicSize;
        }

        void Update()
        {
            float mouseScroll = Input.GetAxis("Mouse ScrollWheel");
            float zoomDirection = 0f;
            // определяем направление зума
            if (Input.GetKey(KeyCode.LeftShift) || mouseScroll > 0)
                zoomDirection = 1.0f;
            else
                zoomDirection = -1.0f;
            // определяем нажатие клавиши Z или колёсика мыши
            if (Input.GetKey(KeyCode.Z) || mouseScroll != 0)
                Zoom(zoomDirection);
        }
    }
}