﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace UnityBaseClasses
{
    /// <summary>
    /// Карта уровня 2D игры
    /// </summary>
    /// <typeparam name="T">Класс, который описывает позицию карты</typeparam>
    public class LevelMap2D<T>
    {
        /// <summary>
        /// Карта
        /// </summary>
        protected T[,] datas = null;
        /// <summary>
        /// Свободные ячейки карты
        /// </summary>
        protected List<IntVector2> availableCell = null;
        /// <summary>
        /// Размер карты
        /// </summary>
        protected IntVector2 size = IntVector2.zero; 

        /// <summary>
        /// Количество доступных ячеек карты
        /// </summary>
        /// <returns>Количество доступных ячеек карты</returns>
        public int GetAvailableCellCount()
        {
            return availableCell.Count;
        }

        /// <summary>
        /// Инициализация
        /// </summary>
        /// <param name="xSize">Размер по горизонтали</param>
        /// <param name="ySize">Размер по вертекали</param>
        /// <param name="defaultValue">Значение по умолчению для ячейки карты</param>
        virtual public void Initialization(int xSize, int ySize, T defaultValue)
        {
            size.x = xSize;
            size.y = ySize;

            datas = new T[xSize, ySize];
            availableCell = new List<IntVector2>();

            for (int i = 0; i < xSize; i++)
            {
                for (int j = 0; j < ySize; j++)
                {
                    datas[i, j] = defaultValue;
                    availableCell.Add(new IntVector2(i, j));
                }
            }
        }

        /// <summary>
        /// Очистка карты
        /// </summary>
        public void RemoveAll()
        {
            availableCell.Clear();
            Array.Clear(datas, 0, datas.Length);
        }

        /// <summary>
        /// Получение индекса свободной ячейки - рандом
        /// </summary>
        /// <returns>Свободная ячейка</returns>
        public IntVector2 GetRandomIndex()
        {
            IntVector2 res = IntVector2.error;
            try
            {
                if (availableCell.Count > 0)
                {
                    int index = Mathf.RoundToInt(UnityEngine.Random.Range(0, availableCell.Count));
                    res = availableCell[index];
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return res;
        }

        /// <summary>
        /// Изменение ячейки карты
        /// </summary>
        /// <param name="pos">Позиция в карте</param>
        /// <param name="data">Новое значение</param>
        /// <param name="needRemove">Удалить или добавить в массив свободных ячеек</param>
        /// <returns></returns>
        virtual public bool ChangeItem(IntVector2 pos, T data, bool needRemove)
        {
            return ChangeItem(pos.x, pos.y, data, needRemove);
        }

        /// <summary>
        /// Изменение ячейки карты
        /// </summary>
        /// <param name="xPos">Позиция в карте</param>
        /// <param name="yPos">Позиция в карте</param>
        /// <param name="data">Новое значение</param>
        /// <param name="needRemove">Удалить или добавить в массив свободных ячеек</param>
        /// <returns></returns>
        virtual public bool ChangeItem(int xPos, int yPos, T data, bool needRemove)
        {
            bool res = false;
            try
            {
                datas[xPos, yPos] = data;
                if (needRemove)
                {
                    IntVector2 tmp = availableCell.Find(item => (item.x == xPos && item.y == yPos));
                    if (tmp != null)
                        availableCell.Remove(tmp); // ячейка занята
                    res = true;
                }
                else
                {
                    HideItem(xPos, yPos); // ячейка свободна
                    res = true;
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return res;
        }

        /// <summary>
        /// Получение значения ячейки карты
        /// </summary>
        /// <param name="xPos">Позиция по Х</param>
        /// <param name="yPos">Позиция по У</param>
        /// <returns>Значение ячейки</returns>
        public T GetItem(int xPos, int yPos)
        {
            T res = default(T);
            try
            {
                res = datas[xPos, yPos];
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return res;
        }

        /// <summary>
        /// Получение значения ячейки карты
        /// </summary>
        /// <param name="pos">Позиция</param>
        /// <returns>Значение ячейки</returns>
        public T GetItem(IntVector2 pos)
        {
            return GetItem((int)pos.x, (int)pos.y);
        }

        /// <summary>
        /// Скрыть элемент карты
        /// </summary>
        /// <param name="xPos">Позиция по Х</param>
        /// <param name="yPos">Позиция по У</param>
        /// <returns>Успех/нет</returns>
        public bool HideItem(int xPos, int yPos)
        {
            bool res = false;
            try
            {
                availableCell.Add(new IntVector2(xPos, yPos));
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return res;
        }

        /// <summary>
        /// Скрыть элемент карты
        /// </summary>
        /// <param name="pos">Позиция</param>
        /// <returns>Успех/нет</returns>
        public bool HideItem(IntVector2 pos)
        {
            return HideItem((int)pos.x, (int)pos.y);
        }
    }
}
