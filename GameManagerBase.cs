﻿// Copyright (c) by Roman Kondratiev
// http://romweb.ru
// kondratiev@gmail.com

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace UnityBaseClasses
{
    /// <summary>
    /// Возможные статусы игры
    /// </summary>
    public enum GameState { Runing, Paused, Restarted, Ended };
    /// <summary>
    /// Возможные статусы героя
    /// </summary>
    public enum HeroState { Normal, Killed };

    /// <summary>
    /// Базовый класс менеджера игры
    /// </summary>

    public class GameManagerBase : MonoBehaviour
    {
        /// <summary>
        /// Разрешение отладочного лога
        /// </summary>
        [Header("Настройка игры")]
        [Tooltip("Разрешение отладочного лога")]
        public bool enableDebugLog = false;
        
        /// <summary>
        /// Класс, который выводит в Debug.Log
        /// </summary>
        [HideInInspector]
        public LogMessage log = null;

        /// <summary>
        /// Свойство - версия игры
        /// </summary>
        protected string GameVersion { get; set; }

        /// <summary>
        /// Ссылка на объект главного героя. Инициализация не тут, а где-то в другом коде!
        /// </summary>
        [HideInInspector]
        public Transform heroTransformRef = null;

        /// <summary>
        /// Свойство - статус игры
        /// </summary>
        public GameState gameState { get; set; }
        /// <summary>
        /// Свойство - статус игрока
        /// </summary>
        public HeroState heroState { get; set; }
        /// <summary>
        /// Свойство - игровые очки
        /// </summary>
        private int Score { get; set; }
        
        /// <summary>
        /// TimeScale в момент постановки игры на паузу
        /// </summary>
        protected float pauseTimeScale = 0;
        /// <summary>
        /// Время старта игры
        /// </summary>
        private float startLevelTime = 0;
        /// <summary>
        /// Время окночания игры
        /// </summary>
        private float endLevelTime = 0;
        /// <summary>
        /// Интервал времени, когда игра находилась на паузе
        /// </summary>
        private float pauseLevelTime = 0;

        private AudioSource backgroundSound = null;

        [HideInInspector] public bool InitializationCompleted = false;

        /// <summary>
        /// Инициализация игры. Используется в Awake и StartGame
        /// </summary>
        virtual public void Initialization()
        {
            log = gameObject.AddComponent<LogMessage>();
            if (log == null)
                Debug.LogError("GameManagerBase.log isn't defined!");
            else
                log.Enable = enableDebugLog;

            GameVersion = SetGameVersion();

            InitializationCompleted = true;
        }
        
        /// <summary>
        /// Задание версии игры. Переопределить и вернуть строку-версию! 
        /// </summary>
        /// <returns>Версия игры</returns>
        virtual public string SetGameVersion()
        {
            return String.Empty;
        }

        void OnDestroy()
        {
            DestroyFunc();
        }

        /// <summary>
        /// Для удаления объектов мененжера игры. Переопределить при необходимости!
        /// </summary>
        virtual public void DestroyFunc()
        {
        }

        /// <summary>
        /// Сброс игровых параметров
        /// </summary>
        virtual public void ResetGameParams()
        {
            gameState = GameState.Runing;
            heroState = HeroState.Normal;
            Score = 0;
            pauseTimeScale = 0;
            startLevelTime = 0;
            endLevelTime = 0;
            pauseLevelTime = 0;
        }

        /// <summary>
        /// Пауза игры
        /// </summary>
        virtual public void PauseGame()
        {
            pauseTimeScale = Time.timeScale;
            Time.timeScale = .0f;
            gameState = GameState.Paused;
        }

        /// <summary>
        /// Уничтожение главного героя игры
        /// </summary>
        virtual public void KillHero()
        {
            if (heroState != HeroState.Killed)
            {
                heroState = HeroState.Killed;
            }
        }

        /// <summary>
        /// Конец игры
        /// </summary>
        virtual public void EndGame()
        {
            gameState = GameState.Ended;
            endLevelTime = Time.timeSinceLevelLoad;
        }

        /// <summary>
        /// Старт игры
        /// </summary>
        /// <param name="resetParams">Сбрасывать параметры или нет</param>
        virtual public void StartGame(bool resetParams = false, bool usePauseState = true)
        {
            if (resetParams)
                ResetGameParams();

            if (gameState == GameState.Paused && usePauseState)
            {
                pauseLevelTime += (Time.timeScale - pauseTimeScale);

                Time.timeScale = pauseTimeScale;
                pauseTimeScale = 0;
            }

            gameState = GameState.Runing;
            startLevelTime = Time.timeSinceLevelLoad;
        }

        /// <summary>
        /// Получение времени прохождения игры
        /// </summary>
        /// <param name="gameStarted">Если true, то используется timeSinceLevelLoad в качестве текущего времени, иначе - endLevelTime</param>
        /// <returns></returns>
        public float GetLevelTime(bool gameStarted = false)
        {
            return (gameStarted ? Time.timeSinceLevelLoad : endLevelTime) - startLevelTime - pauseLevelTime;
        }

        /// <summary>
        /// Сброс времени старта игры
        /// </summary>
        public void ResetLevelTime()
        {
            startLevelTime = Time.timeSinceLevelLoad;
        }

        /// <summary>
        /// Получение времени прохождения игры как строка
        /// </summary>
        /// <param name="gameStarted">Если true, то используется timeSinceLevelLoad в качестве текущего времени, иначе - endLevelTime</param>
        /// <returns></returns>
        public string GetLevelTimeAsString(bool gameStarted = false)
        {
            float time = GetLevelTime(gameStarted);
            string res = String.Format("{0:00}:{1:00}",
                (int)(time / 60), (int)(time % 60));

            return res;
        }

        /// <summary>
        /// Разрешены ли звуковые эффекты - переопределить эту функцию!
        /// </summary>
        /// <returns>Разрешены/нет</returns>
        virtual public bool IsSoundEffectsEnabled()
        {
            return true;
        }

        /// <summary>
        /// Разрешена ли фоновая музыка - переопределить эту функцию!
        /// </summary>
        /// <returns>Разрешены/нет</returns>
        virtual public bool IsBackgroundMusicEnabled()
        {
            return true;
        }

        /// <summary>
        /// Проиграть звук эффект
        /// </summary>
        /// <param name="clip">Аудиоклип</param>
        /// <param name="source">Источник звука</param>
        public void PlaySoundEffect(AudioClip clip, AudioSource source, float volume = -1f)
        {
            if (IsSoundEffectsEnabled() && source && clip)
            {
                if (volume != -1f)
                    source.volume = volume;
                source.PlayOneShot(clip);
            }
        }

        public void PlaySoundEffect(AudioSource source, float volume = -1f)
        {
            if (IsSoundEffectsEnabled() && source)
            {
                if (volume != -1f)
                    source.volume = volume;
                source.Play();
            }
        }

        /// <summary>
        /// Запустить фон. музыку
        /// </summary>
        /// <param name="delayTime">Задержка</param>
        public void PlayBackgroundMusic(float delayTime, AudioSource source = null, float volume = -1f)
        {
            if (source)
                backgroundSound = source;
            else if (backgroundSound == null)
                backgroundSound = gameObject.GetComponent<AudioSource>();

            if (IsBackgroundMusicEnabled())
            {
                if (backgroundSound)
                {
                    if (volume != -1f)
                        backgroundSound.volume = volume;

                    backgroundSound.PlayDelayed(delayTime);
                }
            }
        }

        /// <summary>
        /// Совсем остановить фон. музыку
        /// </summary>
        public void StopBackgroundMusic()
        {
            if (backgroundSound)
                backgroundSound.Stop();
        }

        /// <summary>
        /// Пауза для фоновой музыки
        /// </summary>
        public void PauseBackgroundMusic()
        {
            if (backgroundSound)
                backgroundSound.Pause();
        }

        /// <summary>
        /// Получить кол-во очков
        /// </summary>
        /// <returns>кол-во очков</returns>
        public int GetScore()
        {
            return Score;
        }

        /// <summary>
        /// Изменить кол-во очков
        /// </summary>
        /// <param name="score">Новое кол-во очков</param>
        public void SetScore(int score)
        {
            Score = score;
        }

        /// <summary>
        /// Увеличить очки
        /// </summary>
        /// <param name="scoreValue">Значение, на которое будет увеличение</param>
        virtual public void AddScore(int scoreValue)
        {
            Score += scoreValue;
        }
    }
}