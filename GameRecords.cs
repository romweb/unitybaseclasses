﻿using UnityEngine;
using System.Collections;
using UnityBaseClasses;

namespace UnityBaseClasses
{
    /// <summary>
    /// Работа с рекордами игры
    /// </summary>
    /// <typeparam name="T">Класс, описывающий переменные рекордов. По-умолчанию можно использовать GameRecordsBase.</typeparam>
    public class GameRecords<T> : GameOptions<T>
    {
        /// <summary>
        /// Переопределённая функция получения версии игры, сохранённой в PlayerPrefs
        /// </summary>
        /// <param name="options">Параметры, прочитанные из PlayerPrefs</param>
        /// <returns>Версия игры</returns>
        override public string GetVersionFromRegistry(T options)
        {
            // Получаем версию параметров игры, сохранённую в настройках
            GameRecordsBase baseOptions = new GameRecordsBase();
            baseOptions = options as GameRecordsBase;

            return baseOptions.Version;
        }
    }
}