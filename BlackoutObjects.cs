﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityBaseClasses;

namespace UnityBaseClasses
{
    /// <summary>
    /// Плавное появление объекта
    /// </summary>
    public class BlackoutObjects : MonoBehaviourBase
    {
        /// <summary>
        /// Коэффицент максимальной непрозрачности при старте (1 - полностью не прозрачный)
        /// </summary>
        public float startAlphaColor = 0f;
        /// <summary>
        /// Коэффицент максимальной непрозрачности при старте (1 - полностью не прозрачный)
        /// </summary>
        public float endAlphaColor = 0.8f;
        /// <summary>
        /// Шаг изменения прозрачности
        /// </summary>
        public float stepAlphaColor = .02f;
        /// <summary>
        /// Направление проявления
        /// </summary>
        public float direction = 1f;
        /// <summary>
        /// Пауза перед стартом проявления
        /// </summary>
        public float pauseAsStart = 2f;

        private bool isCreated = false;

        private Image render = null;

        void Awake()
        {
            render = GetComponent<Image>();
            if (render == null)
                Debug.LogError("BlackoutObjects.render isn't defined!");
        }

        void Start()
        {
            Color tempColor = render.material.GetColor("_Color");
            tempColor.a = startAlphaColor;
            render.material.color = tempColor;

            Invoke("Invoke_StartBlackout", pauseAsStart);
        }

        void Invoke_StartBlackout()
        {
            StartBlackout(endAlphaColor);
        }

        void OnDestroy()
        {
            StopCoroutine("Coroutine_BlackoutObjects");
        }

        /// <summary>
        /// Запуск затемнения или осветления экрана
        /// </summary>
        public void StartBlackout(float endAlpha)
        {
            if (!isCreated) // обеспечиваем запуск только одного корутина
            {
                isCreated = true;
                StartCoroutine(Coroutine_BlackoutObjects(endAlpha));
            }
        }

        /// <summary>
        /// Виртуальная функция, вызывется после завершения проявления - переопределить!
        /// </summary>
        virtual public void BlackoutCompleted()
        {

        }
    
        IEnumerator Coroutine_BlackoutObjects(float endAlpha)
        {
            bool needChange = true;
            while (needChange)
            {
                Color tempColor = render.material.GetColor("_Color");
                if (direction == 1 ? (tempColor.a > endAlpha) : (tempColor.a < endAlpha))
                {
                    needChange = false;
                    break;
                }
                else
                {
                    tempColor.a += stepAlphaColor * direction;
                    render.material.color = tempColor;
                }

                yield return new WaitForEndOfFrame();
            }

            BlackoutCompleted();
            isCreated = false;
            yield return null;
        }
    }
}