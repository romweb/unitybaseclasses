﻿// Copyright (c) by Roman Kondratiev
// http://romweb.ru
// kondratiev@gmail.com

using UnityEngine;
using System.Collections;
using UnityBaseClasses;

namespace UnityBaseClasses
{
    /// <summary>
    /// Сдвиг нескольких бакграундов с параллакс-эффектом.
    /// </summary>
    /// <remarks>
    /// В Unity: создать пустой объект, в нем создать несколько Plane, назначить на
    /// них бакграунды. Этот скрипт назначить на родителя и в инспекторе задать "Paralax settings".
    /// У бакграундов должно стоять свойство "Repeat"!
    /// </remarks>
    public class Parallax : MonoBehaviourBase
    {
        /// <summary>
        /// Класс, описываюший бакграунд
        /// </summary>
        [System.Serializable]
        public class Image
        {
            /// <summary>
            /// Компонет Renderer объекта (как правило, это Plane), на который закреплён бакграунд
            /// </summary>
            [Tooltip("Link to component Renderer")]
            public Renderer renderer = null;
            /// <summary>
            /// Коээффицент скорости смещения
            /// </summary>
            [Tooltip("Coefficient offset background")]
            public float scrollSpeed = .0f;

            internal Vector2 savedOffset;
            internal float target = -1f;
         }

        /// <summary>
        /// Массив бакграундов
        /// </summary>
        [Header("Paralax settings")]
        [Tooltip("Paralax images")]
        public Image[] images;

        public float textureOfset = 5f;

        [HideInInspector]
        public float speedFactor = 1f;

        void Start ()
        {
            foreach (Image image in images)
            { 
                image.savedOffset = image.renderer.material.mainTextureOffset;
                image.target = image.renderer.material.mainTextureOffset.x + textureOfset * image.scrollSpeed;
            }

            StartCoroutine("Coroutine_BackgroundChange");
        }

        IEnumerator Coroutine_BackgroundChange()
        {
            do
            {
                yield return new WaitForEndOfFrame();

                foreach (Image image in images)
                {
                    if ((image.target - image.renderer.material.mainTextureOffset.x) < 0.1f)
                        image.target = image.renderer.material.mainTextureOffset.x + textureOfset * image.scrollSpeed * Time.deltaTime * speedFactor;

                    image.renderer.material.mainTextureOffset = new Vector2 (Mathf.Lerp(image.renderer.material.mainTextureOffset.x, image.target, .5f), image.savedOffset.y);
                }
            } while (true);
        }

        void OnDisable()
        {
            foreach (Image image in images)
                image.renderer.material.mainTextureOffset = image.savedOffset;

            StopCoroutine("Coroutine_BackgroundChange");
        }
    }
}