﻿// Copyright (c) by Roman Kondratiev
// http://romweb.ru
// kondratiev@gmail.com

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace UnityBaseClasses
{
    /// <summary>
    /// Описание элемента списка рекордов игры
    /// </summary>
    public class RecordItem : IComparable<RecordItem>
    {
        public RecordItem()
        {
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="time">Время рекорда</param>
        /// <param name="value">Значение рекорда</param>
        /// <param name="levelTime">Время за которое рекорд был достигнут</param>
        public RecordItem(DateTime time, int value, string levelTime, string userName)
        {
            this.time = time;
            this.value = value;
            this.levelTime = levelTime;
            this.userName = userName;
        }

        /// <summary>
        /// Свойство - время рекорда 
        /// </summary>
        public DateTime time { get; set; }
        /// <summary>
        /// Свойство - значение рекорда
        /// </summary>
        public int value { get; set; }
        /// <summary>
        /// Свойство - время за которое рекорд был достигнут
        /// </summary>
        public string levelTime { get; set; }
        /// <summary>
        /// Свойство - имя юзера
        /// </summary>
        public string userName { get; set; }

        /// <summary>
        /// Функция сортировки по времени рекорда
        /// </summary>
        /// <param name="comparePart">Сравниваемый элемент</param>
        /// <returns>см. CompareTo</returns>
        public int CompareTo(RecordItem comparePart)
        {
            if (comparePart == null)
                return 1;
            else
                return comparePart.time.CompareTo(this.time);
        }
    }

    /// <summary>
    /// Класс, для хранения параметров рекордов игры
    /// </summary>
    [System.Serializable]
    public class GameRecordsBase
    {
        /// <summary>
        /// Версия игры
        /// </summary>
        public string Version;
        /// <summary>
        /// Максимальный рекорд игры
        /// </summary>
        public int MaxRecord;
        /// <summary>
        /// Список рекордов
        /// </summary>
        public List<RecordItem> Items;
    }
}