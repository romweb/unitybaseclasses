﻿// Copyright (c) by Roman Kondratiev
// http://romweb.ru
// kondratiev@gmail.com

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace UnityBaseClasses
{
    /// <summary>
    /// Пул объектов
    /// </summary>
    /// <typeparam name="T">Данные объекта</typeparam>
    public class ObjectPoll<T>
    {
        private List<T> objects = null;
        private List<int> availableObjectsIndex = null;

        /// <summary>
        /// Возвращает размер пула
        /// </summary>
        public int Size
        {
            get
            {
                return objects.Count;
            }
        }

        /// <summary>
        /// Функция инициализации
        /// </summary>
        public void Initialization()
        {
            objects = new List<T>();
            availableObjectsIndex = new List<int>();
        }

        /// <summary>
        /// Удаление огбъектов пула
        /// </summary>
        public void RemoveAll()
        {
            objects.Clear();
            availableObjectsIndex.Clear();
        }

        /// <summary>
        /// Добавление объекта в пул
        /// </summary>
        /// <param name="item">Объект</param>
        /// <returns>Индекс добавленного объекта</returns>
        virtual public int Add(T item)
        {
            int res = -1;

            try
            {
                objects.Add(item);
                res = objects.Count - 1;
                ToPoll(res);
            }
            catch (IndexOutOfRangeException e)
            {
                Debug.LogException(e);
            }

            return res;
        }

        /// <summary>
        /// Получение данных объекта из пула
        /// </summary>
        /// <param name="index">Индекс объекта</param>
        /// <returns>Объект</returns>
        public T Get(int index)
        {
            try
            {
                return objects[index];
            }
            catch (IndexOutOfRangeException e)
            {
                Debug.LogException(e);
            }

            return default(T);
        }

        /// <summary>
        /// Изъятие объекта из пула
        /// </summary>
        /// <param name="index">Индекс объекта</param>
        public void FromPool(int index)
        {
            availableObjectsIndex.RemoveAt(index);
        }

        /// <summary>
        /// Возвращение объекта в пул
        /// </summary>
        /// <param name="index">Индекс объекта</param>
        public void ToPoll(int index)
        {
            availableObjectsIndex.Add(index);
        }

        /// <summary>
        /// Получение случайного объекта из пула
        /// </summary>
        /// <param name="poolIndex">Возвращается индекс объекта в пуле</param>
        /// <returns>Индекс объекта в списке объектов</returns>
        public int GetRandomIndex(out int poolIndex)
        {
            int res = poolIndex = -1;

            try
            {
                if (availableObjectsIndex.Count > 0)
                {
                    poolIndex = Mathf.RoundToInt(UnityEngine.Random.Range(0, availableObjectsIndex.Count));
                    res = availableObjectsIndex[poolIndex];
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return res;
        }
    }
}